# LaTeX Essay Template

## Description

This repository provides the necessary STY-Files for my LaTeX Essay template. I use it for class assignments and other documents with more than a few pages, using a full title page.

## Version Control

The template is version-controlled via Git. If you want to keep that up, it is advised to create a separate branch for the modifications you make (i.e. the actual document you're writing). When there are changes in the template itself, they will be published in the main branch `master`. At that point you may merge them back into your custom branch.

### On clone

```shell
git clone https://github.com/Janwillhaus/latex-worksheet-template.git
git checkout git checkout -b workinprogress
```

### On template update

```shell
git stash
git pull --rebase origin master
git stash apply
```


## Basic outline / functionality

The template relies on a few functions to be executed at specific points of the document. The variables shall be defined right after the `usepackage` command, including the worksheet template. After that, there's basically only the title section to be created. Thus, a minimal example derives:

```latex
\documentclass{scrartcl}
\usepackage{Jw_Worksheet}
\usepackage[ngerman]{babel}

\title{}
\assignment{}
\course{}
\lecturer{}

\author{}
\matnumber{}
\datereturned{\today}

\loadpackages

\usepackage{microtype}
\begin{document}
\maketitle

% Yadda yadda yadda

\end{document}
```


## Licence

Copyright (c) 2014 Jan Willhaus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
